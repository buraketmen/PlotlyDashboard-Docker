FROM ubuntu:latest
RUN apt-get update -y && \
    apt-get install -y python-pip python-dev
RUN apt-get upgrade -y

COPY ./requirements.txt /app/requirements.txt
WORKDIR /app
RUN pip install -r requirements.txt
COPY . /app
ENTRYPOINT [ "python" ]

CMD [ "index.py" ]